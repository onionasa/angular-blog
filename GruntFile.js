module.exports = function (grunt)
{
	var proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest;
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			options: {
				separator: "\n\n"
			},
			deps: {
				src: [
				'bower_components/angular/angular.min.js',
				'bower_components/angular-route/angular-route.min.js',
				'bower_components/angular-resource/angular-resource.min.js',
				'bower_components/angular-cookies/angular-cookies.min.js',
				'bower_components/jquery/dist/jquery.min.js',
				'bower_components/bootstrap/dist/js/bootstrap.min.js',
				'bower_components/ng-idle/angular-idle.min.js',
				'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js'

				],
				dest: 'src/js/<%= pkg.name %>-deps.js'
			},
			chart:{
				src: [
				'bower_components/Chart.js/Chart.min.js',
				'bower_components/angular-chart.js/dist/angular-chart.js',
				],
				dest: 'src/js/<%= pkg.name %>-chart.js'
			},
			myJs:{
				src: [
				'app.js',
				'myjs/services.js',
				'myjs/controllers.js'
				],
				dest: 'src/js/<%= pkg.name %>-main.js'
			},
			css: {
				src: ['bower_components/bootstrap/dist/css/bootstrap.min.css',
				'src/css/style.css','bower_components/angular-chart.js/dist/angular-chart.css'
				],
				dest: 'src/css/<%= pkg.name %>.css'
			},
		},
		uglify: {
			options: {
				mangle: false
			},
			my_target: {
				files: {
					'src/js/<%= pkg.name %>-deps-min.js': ['src/js/<%= pkg.name %>-deps.js']
				}
			},
			my_target2: {
				files: {
					'src/js/<%= pkg.name %>-chart-min.js': ['src/js/<%= pkg.name %>-chart.js']
				}
			},
			my_target3: {
				files: {
					'src/js/<%= pkg.name %>-main-min.js': ['src/js/<%= pkg.name %>-main.js']
				}
			},
		},
		connect: {
			'static': {
				options: {
					hostname: 'localhost',
					port: 9001,
				},
				proxies: [{
					context:'/',
					host: 'localhost',
					port: 8001,
					https: false,
					xforward: false,

				},]
			},
			server: {
				options: {
					context:'/api',
					hostname: "localhost",
					port:9000,
					keepalive: true,
					open: true,
					middleware: function (connect, options) {
						return [proxySnippet];
					}
				},
				proxies: [{
					context:'/api',
					host: 'localhost',
					port: 8000,

				},
				{
					context:'/',
					host: 'localhost',
					port: 9001,

				},


				]
			},
		}

	});

	//npm tasks
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-connect-proxy');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-ngdocs');

	//tasks
	grunt.registerTask('default', 'Default Task Alias', ['build','ready']);
	grunt.registerTask('ready','Server Running', ['connect:static',
		'configureProxies:server',
		'connect:server']);
	grunt.registerTask('build', 'Build the application', 
		[
		'concat',
		'uglify'
		]);
}
