var myApp =angular.module('app', ['ngRoute','ngResource','ngCookies','ngIdle','ui.bootstrap','chart.js']);
myApp.config(['$routeProvider','IdleProvider','KeepaliveProvider',function($routeProvider,IdleProvider,KeepaliveProvider) {
	$routeProvider
	.when('/home', {
		templateUrl:'src/views/home.html',
		controller:'postCtr'
	})
	.when('/category/:id',{
		templateUrl:'src/views/category.html',
		controller:'postCtr'
	})
	.when('/post/:id',{
		templateUrl:'src/views/postdetail.html',
		controller:'postCtr'
	})
	.when('/register',{
		templateUrl:'src/views/register.html',
		controller:'registerCtr'
	})
	.when('/settings',{
		templateUrl:'src/views/settings.html',
		controller:'registerCtr'
	})
	.when('/login',{
		templateUrl:'src/views/login.html',
		controller:'loginCtr'
	})
	.when('/catmanager',{
		templateUrl:'src/views/catmanager.html',
		controller:'catmanagerCtr'
	})
	.when('/add_cat',{
		templateUrl:'src/views/add_category.html',
		controller:'catmanagerCtr'
	})
	.when('/edit_cat',{
		templateUrl:'src/views/edit_category.html',
		controller:'catmanagerCtr'
	})
	.when('/delete_cat',{
		templateUrl:'src/views/delete_category.html',
		controller:'catmanagerCtr'
	})
	.when('/postmanager',{
		templateUrl:'src/views/postmanager.html',
		controller:'postmanagerCtr'
	})
	.when('/add_post',{
		templateUrl:'src/views/add_post.html',
		controller:'postmanagerCtr'
	})
	.when('/edit_post',{
		templateUrl:'src/views/edit_post.html',
		controller:'postmanagerCtr'
	})
	.when('/delete_post',{
		templateUrl:'src/views/delete_post.html',
		controller:'postmanagerCtr'
	})
	.when('/stats',{
		templateUrl:'src/views/stats.html'

	})
	.otherwise({
		redirectTo: '/home'
	});
	IdleProvider.idle(5);
	IdleProvider.timeout(5);
	KeepaliveProvider.interval(10);
}]).run(['$cookieStore','$rootScope','Idle','$modal','loginService', function($cookieStore,$rootScope,Idle,$modal,loginService){
	$rootScope.$on('IdleStart', function() {
		$rootScope.t= $modal.open({
			templateUrl: 'src/views/warn.html',
			windowClass: 'modal-danger'
		});
    	//window.location="#/login"
    });
	$rootScope.$on('IdleTimeout', function() {
		$rootScope.t.close();
		loginService.logoutUser(function(data){
			window.location="#/login"
		});

	});
	$rootScope.$on('IdleEnd', function() {
		$rootScope.t.close();
	});

	if($cookieStore.get('token')){
		$rootScope.cfg={
			headers: {'Authorization': ['Token '+$cookieStore.get('token')]}
		};
		$rootScope.auth="true";
		$rootScope.noauth="false";
		$rootScope.user=$cookieStore.get('user');
	}else{
		$rootScope.auth="false";
		$rootScope.noauth="true";
		$rootScope.user="";
	}
	
}])

