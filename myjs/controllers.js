



myApp.directive('testdir', function(){
	// Runs during compile
	var tn = 'src/views/navbar.html';
	return {
		restrict: 'E',
		templateUrl:tn,
	};
});

myApp.controller('navbarCtr', ['$scope','$rootScope','categoryService','loginService','$route','$templateCache','Idle', 
	function($scope,$rootScope,categoryService,loginService,$route,$templateCache,Idle){
	categoryService.getCatList(function(data){
		$scope.catList=data;
	});	
	$scope.logout=function() {
					Idle.unwatch();
		loginService.logoutUser(function(data){
			if(data.success){
				window.location="#/login";
			}
		});
		
	};
}]);


myApp.controller('postCtr', ['$scope','$rootScope','postService','registerService','$http', 
	function($scope,$rootScope,postService,registerService,$http){
		$rootScope.test="false";
		$scope.cmt={};
		$scope.cmt.vote="true";

		registerService.getUser(function(data){
			$scope.fn=data.first_name;
		})
		$scope.comment_result_type=null;
		$scope.comment_result_success=false;
		$scope.reset=function() {
			$scope.comment_result_type=null;
			$scope.comment_result_success=false;
		}
		$scope.yolla = function(id,cmt){
			postService.sendComment(id,cmt,function(data){
				if(data.info){
					$scope.comment_result=data.info;
					$scope.comment_result_success=true;
					$scope.comment_result_type="danger";
				}else{
					$scope.comment_result="Comment succcessfully sended!";
					$scope.comment_result_success=true;
					$scope.comment_result_type="success";
					window.location="#/home";
				}

			});	
		};

		$scope.cmtVerify=function(pk,ck) {
			postService.verifyComment(pk,ck,function(data){
				window.location="#/home";
			});
		};
		$scope.cmtDelete=function(pk,ck) {
			postService.deleteComment(pk,ck,function(data){
				window.location="#/home";
			});
		};

		postService.getPostList(function(data){
			$scope.posts=data;
		});	
		postService.getCatPostList(function(data){
			$scope.catposts=data;
		});

		postService.getPostDetail(function(data){
			$scope.postdetail=data;
		});
		postService.getPostComments(function(data){
			if(data.comment_isValid=="True"){
				$scope.cmtVrfy="false";
			}else{
				$scope.cmtVrfy="true";
			}

			$scope.comments=data;
		});
	}]);


myApp.controller('registerCtr', ['$scope','registerService','loginService','$rootScope', 
	function($scope,registerService,loginService,$rootScope){
		if($rootScope.user){
		window.location="#/settings";
	}

	$scope.ud = {};
	$scope.uud={};
	$scope.reset=function() {
		$scope.reg_result_type=null;
		$scope.reg_result_success=false;
	}
	registerService.getUser(function(data){
		$scope.uud=data;
		$scope.uud.password="";
	})

	$scope.updateUser=function(uud) {
		registerService.updateUser(uud,function(data){
			if(data.info){
				$scope.reg_result=data;
				$scope.reg_result_success=true;
				$scope.reg_result_type="danger";
			}else{
				$scope.reg_result="You have to login";
				$scope.reg_result_success=true;
				$scope.reg_result_type="success";
				loginService.logoutUser(function(data){
					$scope.lg="User logout!"
					window.location="#/login"
				});
			}
		});
	};

	$scope.reg=function(ud) {
		registerService.registerUser(ud,function(data){
			if(data.info){
				$scope.reg_result="User succcessfully registered!";
				$scope.reg_result_success=true;
				$scope.reg_result_type="success";
				loginService.loginUser(ud,function(data){
					if(data.info){
						$scope.reg_result=data.info;
						$scope.reg_result_success=true;
						$scope.reg_result_type="danger";

					}else{
						$scope.reg_result="User succcessfully loged in!";
						$scope.reg_result_success=true;
						$scope.reg_result_type="success";
						window.location="#/home";
					}
				});
			}else{
				$scope.reg_result=data;
				$scope.reg_result_success=true;
				$scope.reg_result_type="danger";
			}
		});
	};
	
}])

myApp.controller('loginCtr', ['$scope','$rootScope','loginService','$cookieStore','Idle', 
	function($scope,$rootScope,loginService,$cookieStore,Idle){
		$scope.ud = {};
		$scope.reset=function() {
			$scope.reg_result_type=null;
			$scope.reg_result_success=false;
		}
		$scope.login=function(ud) {
			Idle.watch();
			$scope.reset();
			loginService.loginUser(ud,function(data){
				if(data.info){
					$scope.reg_result=data.info;
					$scope.reg_result_success=true;
					$scope.reg_result_type="danger";

				}else{
					$scope.reg_result="User succcessfully loged in!";
					$scope.reg_result_success=true;
					$scope.reg_result_type="success";
					window.location="#/home";
				}
			})


		}

		$scope.logout=function() {

			loginService.logoutUser(function(data){
				$scope.lg="User logout!"
			});
		};

	}])



myApp.controller('catmanagerCtr', ['$scope','$rootScope','catmanagerService', function($scope,$rootScope,catmanagerService){
	if(!$rootScope.user){
		window.location="#/login";
	}
	catmanagerService.getCatManagerList(function(data) {
		$scope.catlist=data;
	});
	$scope.catname=catmanagerService.getCat();
	$scope.edit=function(id,cat) {
		catmanagerService.setCat(cat);
		catmanagerService.setID(id);
		window.location="#/edit_cat"

	}
	$scope.dlt=function(id){
		catmanagerService.setID(id);
		window.location="#/delete_cat"
	}
	$scope.dltcat=function(){
		catmanagerService.deleteCat(catmanagerService.getID(),function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{
				window.location="#/catmanager"
			}
		});
	}

	$scope.reset=function() {
		$scope.cat_result_type=null;
		$scope.cat_result_success=false;
		catmanagerService.setCat("");
		catmanagerService.setID("");
	}
	$scope.add=function(cat){
		catmanagerService.createCat(cat,function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{

				$scope.cat_result_success=true;
				$scope.cat_result_type="success";
				window.location="#/catmanager"
			}
		});
	}

	
	$scope.update=function(cat){
		catmanagerService.updateCat(catmanagerService.getID(),cat,function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{

				$scope.cat_result_success=true;
				$scope.cat_result_type="success";
				window.location="#/catmanager"
			}
		});
	}	

}]);


myApp.controller('postmanagerCtr', ['$scope','$rootScope','postmanagerService','categoryService', function($scope,$rootScope,postmanagerService,categoryService){
	if(!$rootScope.user){
		window.location="#/login";
	}
	$scope.pos={};
	$scope.pos.post_tittle=postmanagerService.getPosttittle();
	$scope.pos.post_content=postmanagerService.getPostcontent();
	$scope.pos.post_cat=postmanagerService.getPostcat();
	postmanagerService.getPostManagerList(function(data) {
		$scope.postlist=data;
	});
	categoryService.getCatList(function(data){
		$scope.catList=data;
	});	
	
	$scope.reset=function() {
		$scope.cat_result_type=null;
		$scope.cat_result_success=false;
		postmanagerService.setPosttittle("");
		postmanagerService.setPid("");
		postmanagerService.setPostcontent("");
		postmanagerService.setPostcat("");
	}
	$scope.add=function(pos){
		console.log(pos.post_cat);
		postmanagerService.createPost(pos,function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{
				$scope.cat_result=data;
				$scope.cat_result_success=true;
				$scope.cat_result_type="success";
				window.location="#/postmanager"
			}
		});
	}
	$scope.update=function(pos){
		console.log(pos.pc);
		postmanagerService.updatePost(postmanagerService.getPid(),pos,function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{
				$scope.cat_result=data;
				$scope.cat_result_success=true;
				$scope.cat_result_type="success";
				window.location="#/postmanager"
			}
		});
	}
	$scope.edit=function(id,posttittle,postcontent,postcat) {
		postmanagerService.setPosttittle(posttittle);
		postmanagerService.setPid(id);
		postmanagerService.setPostcontent(postcontent);
		postmanagerService.setPostcat(postcat);
		window.location="#/edit_post"
	}

	$scope.dlt=function(id){
		postmanagerService.setPid(id);
		window.location="#/delete_post"
	}
	$scope.dltpost=function(){
		postmanagerService.deletePost(postmanagerService.getPid(),function(data) {
			if(data.info){
				$scope.cat_result=data.info;
				$scope.cat_result_success=true;
				$scope.cat_result_type="danger";
				
			}else{
				window.location="#/postmanager"
			}
		});
	}

}]);



myApp.controller('ldlCtr', ['$scope','postmanagerService','postService','$rootScope', 
	function($scope,postmanagerService,postService,$rootScope){
		$scope.labels=[];
		$scope.data=[[],[]];
		$scope.series = ['Like','Dislike'];
		postmanagerService.getPostManagerList(function(data) {
			$scope.test=$.map(data, function(el) { return el; });
			for(var i=0;i<data.length;i++){
				$scope.labels.push($scope.test[i].post_tittle);
				$scope.data[0].push($scope.test[i].post_like);
				$scope.data[1].push($scope.test[i].post_dislike);
			}
		});

		$scope.testa=function(arg){
			return arg;
		}
	}]);

