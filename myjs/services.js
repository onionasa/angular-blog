

myApp.service('categoryService',['$http',function($http){

	return {
		getCatList:function(callback){
			$http.get('http://localhost:9000/api/category/').
			success(function(data) {
				callback(data);
			})
			
		},

	}	
}]);

myApp.service('postService',['$http','$routeParams','$rootScope',function($http,$routeParams,$rootScope){

	return {
		getPostList:function(callback){
			$http.get('http://localhost:9000/api/post/').
			success(function(data) {
				callback(data);
			})
			
		},
		getCatPostList:function(callback){
			$http.get('http://localhost:9000/api/catpost/'+$routeParams.id+'/').
			success(function(data) {
				callback(data);
			})
			
		},
		getPostDetail:function(callback){
			$http.get('http://localhost:9000/api/postdetail/'+$routeParams.id+'/').
			success(function(data) {
				callback(data);
			})
		},
		getPostComments:function(callback){
			$http.get('http://localhost:9000/api/comment/'+$routeParams.id+'/',$rootScope.cfg).
			success(function(data) {
				callback(data);
			})
		},
		getPostCommentsID:function(id){
			return $http.get('http://localhost:9000/api/comment/'+id+'/',$rootScope.cfg)
		},
		sendComment:function(id,commment_data,callback){
			$http.post('http://localhost:9000/api/comment/'+id+'/', 
				{ 'email' : commment_data.email,'comment':commment_data.comment,'vote':commment_data.vote }).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);
				});
			},
			verifyComment:function(pk,ck,callback) {
				$http.get('http://localhost:9000/api/comment_verify/'+ck+'/'+pk+'/',$rootScope.cfg 
					).success(function(data) {
						callback(data);
					}).error(function(data){
						callback(data);
					});
				},
				deleteComment:function(pk,ck,callback) {
					$http.get('http://localhost:9000/api/comment_delete/'+ck+'/'+pk+'/',$rootScope.cfg 
						).success(function(data) {
							callback(data);
						}).error(function(data){
							callback(data);
						});
					},

				}	
			}]);

myApp.service('registerService',['$http','$cookieStore','$rootScope',function($http,$cookieStore,$rootScope){

	return {
		registerUser:function(userdata,callback){
			$http.post('http://localhost:9000/api/user_register/', 
				{ 'username' : userdata.username,'password':userdata.password,'email':userdata.mail }).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);
				});
			},
			getUser:function(callback){
				$http.get('http://localhost:9000/api/user_register/'+$cookieStore.get('id')+'/', 
					$rootScope.cfg).success(function(data) {
						callback(data);
					}).error(function(data){
						callback(data);
					});
				},
				updateUser:function(userdata,callback){
					$http.put('http://localhost:9000/api/user_register/'+$cookieStore.get('id')+'/', userdata,
						$rootScope.cfg).success(function(data) {
							callback(data);
						}).error(function(data){
							callback(data);
						});
					},
				}


			}]);

myApp.service('loginService',['$http','$cookieStore','$rootScope',function($http,$cookieStore,$rootScope){

	return {
		loginUser:function(userdata,callback){
			$http.post('http://localhost:9000/api/login/', 
				{ 'username' : userdata.username,'password':userdata.password }).success(function(data) {
					callback(data);
					$cookieStore.put('token', data.token);
					$cookieStore.put('user', data.user);
					$cookieStore.put('id',data.id);
					$rootScope.auth="true";
					$rootScope.noauth="false";
					$rootScope.user=userdata.username;
					$rootScope.cfg={
						headers: {'Authorization': ['Token '+$cookieStore.get('token')]}
					};
				}).error(function(data){
					callback(data);
				});
			},
			logoutUser:function(callback){
				$http.get('http://localhost:9000/api/logout/',$rootScope.cfg 
					).success(function(data) {
						callback(data);
						$cookieStore.remove('token');
						$cookieStore.remove('user');
						$cookieStore.remove('id');
						$rootScope.cfg={};
						$rootScope.auth="false";
						$rootScope.noauth="true";
						$rootScope.user="";

					}).error(function(data){
						callback(data);

					});
				},
			}


		}]);



myApp.service('catmanagerService',['$http','$cookieStore','$rootScope',function($http,$cookieStore,$rootScope){
	var catname;
	var cid;
	return {
		getCatManagerList:function(callback) {
			$http.get('http://localhost:9000/api/catmanager/',$rootScope.cfg 
				).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);

				});
			},
			setCat:function(arg){
				catname=arg;
			},
			getCat:function(){
				return catname;
			},
			setID:function(arg){
				cid=arg;
			},
			getID:function(){
				return cid;
			},
			createCat:function(catname,callback){
				$http.post('http://localhost:9000/api/category/',
					{ 'cat_name' : catname },$rootScope.cfg 
					).success(function(data) {
						callback(data);
					}).error(function(data){
						callback(data);

					});
				},
				updateCat:function(id,catname,callback){
					$http.put('http://localhost:9000/api/category/'+id+'/',
						{ 'cat_name' : catname },$rootScope.cfg 
						).success(function(data) {
							callback(data);
						}).error(function(data){
							callback(data);

						});
					},
					deleteCat:function(id,callback){
						$http.delete('http://localhost:9000/api/category/'+id+'/',$rootScope.cfg 
							).success(function(data) {
								callback(data);
							}).error(function(data){
								callback(data);

							});
						},

					}
				}]);


myApp.service('postmanagerService',['$http','$cookieStore','$rootScope',function($http,$cookieStore,$rootScope){
	var pid;
	var posttittle;
	var postcontent;
	var postcat;
	return {
		setPid:function(arg) {
			pid=arg;
		},
		getPid:function(){
			return pid;
		},
		setPosttittle:function(arg){
			posttittle=arg;
		},
		getPosttittle:function(){
			return posttittle;
		},
		setPostcontent:function(arg) {
			postcontent=arg;
		},
		getPostcontent:function(){
			return postcontent;
		},
		setPostcat:function(arg){
			postcat=arg;
		},
		getPostcat:function(){
			return postcat;
		},
		getPostManagerList:function(callback) {
			$http.get('http://localhost:9000/api/postmanager/',$rootScope.cfg 
				).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);

				});
			},
			createPost:function(posts,callback){
				console.log(posts.post_cat);
				$http.post('http://localhost:9000/api/post/',
					{ 'post_tittle' : posts.post_tittle,'post_content':posts.post_content,'post_cat':posts.post_cat 
				},$rootScope.cfg 
				).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);

				});
			},
			updatePost:function(id,posts,callback){
				$http.put('http://localhost:9000/api/post/'+id+'/',
					{ 'post_tittle' : posts.post_tittle,'post_content':posts.post_content,'post_cat':posts.pc 
				},$rootScope.cfg 
				).success(function(data) {
					callback(data);
				}).error(function(data){
					callback(data);

				});
			},
			deletePost:function(id,callback){
				$http.delete('http://localhost:9000/api/post/'+id+'/',$rootScope.cfg 
					).success(function(data) {
						callback(data);
					}).error(function(data){
						callback(data);

					});
				},

			}
		}]);